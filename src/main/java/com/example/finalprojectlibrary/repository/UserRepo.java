package com.example.finalprojectlibrary.repository;

import com.example.finalprojectlibrary.entity.Book;
import com.example.finalprojectlibrary.entity.Customer;
import com.example.finalprojectlibrary.entity.Rezervations;
import com.example.finalprojectlibrary.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, String> {
    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndValidUser(String username,Boolean validUser);


    @Query("select a from User a where a.username =:username")
    User findByUserNName(@Param("username") String username);



}

