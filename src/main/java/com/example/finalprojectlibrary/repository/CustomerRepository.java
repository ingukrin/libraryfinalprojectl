package com.example.finalprojectlibrary.repository;

import com.example.finalprojectlibrary.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> findByName(String name);
    List<Customer> findByNameContaining(String name);
//    List<Customer> findByNameContainingTop50(String name);
    List<Customer> findByValidCustomer(Boolean valid);

    Customer findByCustId(Long id);

    Optional<Customer> findByEmail(String email);

    @Query("select a from Customer a where a.custId =:id")
    List<Customer> findByCustIdAll(@Param("id") long id);

}
