package com.example.finalprojectlibrary.repository;

import com.example.finalprojectlibrary.entity.Book;
import com.example.finalprojectlibrary.entity.Customer;
import com.example.finalprojectlibrary.entity.Rezervations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RezervationsReposetory extends JpaRepository<Rezervations, Long> {
    List<Rezervations> findByBook(Book book);
    Optional<Rezervations> findByCustomer(Customer customer);
    Optional<Rezervations> findByBookAfter(Book books);
    Rezervations findByRezervationId(Long id);


    @Query("select a from Rezervations a where a.returnDate is null and a.rezervationDate<=CURRENT_TIMESTAMP-14")
    List<Rezervations> unreturnedBooks();


}
