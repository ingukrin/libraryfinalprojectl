package com.example.finalprojectlibrary.repository;

import com.example.finalprojectlibrary.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Long> {
    List<Book> findByName(String name);
    List<Book> findByValidBook(boolean valid);
//    List<Book> findByNameContaining(boolean valid);
    Book findByBookId(Long id);
//    List<Book> findByBookRezervations(String publishHouse);


    @Query("select a from Book a where not exists (select 1 from Rezervations b where a.bookId =b.book.bookId  and b.returnDate is null ) and a.validBook=true and a.bookId =:id")
    List<Book> findByBookIdWioutRezervations(@Param("id") long id);

    @Query("select a from Book a where a.bookId =:id")
    List<Book> findByBookIdAll(@Param("id") long id);

    @Query("select a from Book a where not exists( select 1 from Rezervations  b where a.bookId =b.book.bookId  and b.returnDate is null ) and a.validBook=true and rownum()<50 and a.name like %:title%")
    List<Book> findByTitleIdWioutRezervations(@Param("title") String title);

    @Query("select a from Book a where a.name like %:title% and rownum()<100")
    List<Book> findByTitleAll(@Param("title") String title);

}
