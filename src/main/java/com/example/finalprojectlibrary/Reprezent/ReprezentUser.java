package com.example.finalprojectlibrary.Reprezent;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class ReprezentUser {
    @Id
    @NotEmpty(message = "UserName name is required")
    private String username;
    @NotEmpty(message = "Name is required")
    private String name;
    @NotEmpty(message = "User e-mail is required")
    @Email(message = "e-mail have incorrect format")
    private String email;
    @NotEmpty(message = "Password can not be empty")
    private String password;
    @NotEmpty(message = "Password can not be empty")
    private String passwordRepeat;

}
