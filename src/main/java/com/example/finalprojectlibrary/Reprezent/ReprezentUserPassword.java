package com.example.finalprojectlibrary.Reprezent;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class ReprezentUserPassword {
    @Id
    @NotEmpty(message = "UserName name is required")
    private String username;
    @NotEmpty(message = "Password can not be empty")
    private String oldPassword;
    @NotEmpty(message = "Password can not be empty")
    private String password;
    @NotEmpty(message = "Password can not be empty")
    private String passwordRepeat;

}
