package com.example.finalprojectlibrary.Reprezent;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class ReprezentUserData {
    @Id
    @NotEmpty(message = "UserName name is required")
    private String username;
    @NotEmpty(message = "Name is required")
    private String name;
    @NotEmpty(message = "User e-mail is required")
    @Email(message = "e-mail have incorrect format")
    private String email;
    private Boolean validUser;
    private Long role;
}
