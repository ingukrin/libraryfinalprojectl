package com.example.finalprojectlibrary.Reprezent;

import com.example.finalprojectlibrary.entity.Rezervations;
import com.example.finalprojectlibrary.repository.RezervationsReposetory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class RezervationService {

    private final RezervationsReposetory rezervationRepo;

    public List<Rezervations> getAll() {
        return rezervationRepo.findAll();
    }

    public Rezervations getById(long id) {
        log.info("Tring to find book with id:" + id);
        return rezervationRepo.findByRezervationId(id);
        //  .orElseThrow(() -> new ModelException("File data with id:" + id + " not found!"));
    }
    public void saveRezervations(Rezervations rezervation) {
        rezervationRepo.save(rezervation);
    }
    public List<Rezervations> unreturnedRez() {
        return rezervationRepo.unreturnedBooks();
    }
}
