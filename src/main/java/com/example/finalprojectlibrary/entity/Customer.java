package com.example.finalprojectlibrary.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
//import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long custId;
    @NotEmpty(message = "Customer name is required")
    private String name;
    @NotEmpty(message = "Customer phone is required")
    private String phone;
    @NotEmpty(message = "Customer registration number is required")
    @Column(unique=true)
    private String regnr;
    @NotEmpty(message = "Customer city is required")
    private String city;
    @NotEmpty(message = "Customer address is required")
    private String address;
    @NotEmpty(message = "Customer e-mail is required")
    @Email(message = "e-mail have incorrect format")
    @Column(unique=true)
    private String email;
    private Boolean validCustomer=true;
    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<Rezervations> cutomerRezervations = new ArrayList<>();

}
