package com.example.finalprojectlibrary.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
public class Rezervations {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
 //   private Long bookCustId;
    private Long rezervationId;

    @ManyToOne
    @JoinColumn(name="bookId")
    private Book book;
    @ManyToOne
    @JoinColumn(name="custId")
    private Customer customer;
    //    private Long custId;
    @NotNull
    private Date rezervationDate;
    private Date returnDate;


}