package com.example.finalprojectlibrary.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@RequiredArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long bookId;
    @NotEmpty(message = "Name is required field")
    private String name;
    @NotNull(message = "Year is required field")
    @Min(message = "Must be of Years after 1900 in format YYYY", value = 1900)
    private long year;
    @NotBlank(message = "Publish House is required field")
    private String publishHouse;
    @NotEmpty(message = "Author is required field")
    private String author;
    private boolean validBook=true;
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    private List<Rezervations> bookRezervations = new ArrayList<>();
}
