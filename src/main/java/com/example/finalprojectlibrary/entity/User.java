package com.example.finalprojectlibrary.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @NotEmpty(message = "UserName name is required")
    private String username;
    @NotEmpty(message = "Name is required")
    private String name;
    @NotEmpty(message = "User e-mail is required")
    @Email(message = "e-mail have incorrect format")
    private String email;
    @NotEmpty(message = "Password can not be empty")
    private String password;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_username", referencedColumnName = "username"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;
    private boolean validUser=true;
}
