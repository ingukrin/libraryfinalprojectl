package com.example.finalprojectlibrary.entity;

public enum RoleName {
    ADMIN, USER
}
