package com.example.finalprojectlibrary.controler;

import com.example.finalprojectlibrary.service.BooksService;
import com.example.finalprojectlibrary.service.CustomerService;
import com.example.finalprojectlibrary.Reprezent.RezervationService;
import com.example.finalprojectlibrary.entity.Rezervations;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
//import org.thymeleaf.util.DateUtils;


@Slf4j
@Controller
@RequiredArgsConstructor
public class RezervationControler {
    private final RezervationService rezService;
    private final BooksService booksService;
    private final CustomerService custService;


    @GetMapping
    public String homePage() {
        return  "home";
    }


    @GetMapping("/rezervation/unreturned")
    public String unreturnedBooks( Model model) {
        log.info("Prepear unreturned book list!!!");
        List<Rezervations> rezList = rezService.unreturnedRez();
        model.addAttribute("rezList", rezList);
        return  "rezervations-unreturned";
    }


    @PostMapping("/returnbook/{idr}/{id}")
    public String deleteByReservationNumber(@PathVariable("idr") long idr, @PathVariable("id") long id, Model model) {
        log.info("Trying delete rezervation for  --> Rezervation id --> "+ idr + " FOR CUSTOMER WITH ID" + id);
        java.util.Date date=new java.util.Date();
        log.info(date.toString());
        Rezervations rez = rezService.getById(idr);
        rez.setReturnDate(date);

        rezService.saveRezervations(rez);
        log.info("I hope, that it was saved");
        return  "redirect:/customer/{id}";
    }





    @GetMapping("/rezervation/new/{id}")
    public String startNewReservationForm(@PathVariable("id") long id,  Model model,String title, Long bookid) {
        log.info("Trying to add rezervation for  --> FOR CUSTOMER WITH ID" + id);
        model.addAttribute("customer", custService.getById(id));
        if(title !=  "")
        {
            log.info("Found title --> " + title);
            model.addAttribute("books", booksService.findByTitleIdWioutRezervations(title));
        }
        else if(bookid != null){
            model.addAttribute("books", booksService.findByBookIdWioutRezervations(bookid));
        }
        else
        {
            log.info("Didnt found parametrs");
            model.addAttribute("books", null);
        }
        log.info("Print list of books on screen");
        return  "rezervation_make";
    }

    @PostMapping("/rezervation/add/{custid}/{bookid}")
    public String addNewReservation(@PathVariable("custid") long custid, @PathVariable("bookid") long bookid, Model model) {
        log.info("Trying to add reservation for  --> FOR CUSTOMER WITH cust_id-->" + custid + " Book_id-->"+ bookid);
        Rezervations rez = new Rezervations();
        rez.setBook(booksService.getById(bookid));
        rez.setCustomer(custService.getById(custid));
        java.util.Date date=new java.util.Date();
        rez.setRezervationDate(date);
        rezService.saveRezervations(rez);
        return  "redirect:/customer/{custid}";
    }





}
