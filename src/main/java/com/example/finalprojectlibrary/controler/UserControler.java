package com.example.finalprojectlibrary.controler;


import com.example.finalprojectlibrary.Reprezent.ReprezentUserCreate;
import com.example.finalprojectlibrary.Reprezent.ReprezentUserData;
import com.example.finalprojectlibrary.Reprezent.ReprezentUserPassword;
import com.example.finalprojectlibrary.entity.User;
import com.example.finalprojectlibrary.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserControler {
    private final UserService userService;



    @GetMapping("/edit")
    public String editUserData( Model model, Principal principal) {
        User user = userService.findUser(principal.getName());
        ReprezentUserData reprezentUserData = new ReprezentUserData();
        reprezentUserData.setName(user.getName());
        reprezentUserData.setUsername(user.getUsername());
        reprezentUserData.setEmail(user.getEmail());
        model.addAttribute("reprezentUserData", reprezentUserData);
        return  "user-edit";
    }

    @PostMapping("/edit")
    public String editUserDataPost(Model model, @Valid ReprezentUserData reprezentUserData, Errors errors) {
        if (errors.hasErrors()) {
            model.addAttribute("reprezentUserData", reprezentUserData);
            return  "user-edit";
        }else {
            User user = userService.findUser(reprezentUserData.getUsername());
            reprezentUserData.setValidUser(user.isValidUser());
            userService.updateUser(reprezentUserData);
            return  "redirect:/customer";

        }
    }

    @GetMapping("/password")
    public String editUserPassword(Model model, Principal principal) {
        User user = userService.findUser(principal.getName());
        ReprezentUserPassword reprezentUserPassword = new ReprezentUserPassword();
        reprezentUserPassword.setUsername(user.getUsername());
//        reprezentUserPassword.setOldPassword(user.getPassword());
        model.addAttribute("reprezentUserPassword", reprezentUserPassword);
        return  "user-password";
    }

    @PostMapping("/password")
    public String editUserPasswordPost( final Model model, @Valid ReprezentUserPassword reprezentUserPassword,Errors errors ) {
        log.info("Start password verification");
        if(!reprezentUserPassword.getPassword().equals(reprezentUserPassword.getPasswordRepeat())){
            errors.rejectValue("password","","Password does not much");
            log.info("Password does not much");
            return  "user-password";
        }
        else  if (errors.hasErrors()) {
            log.info("Post username" + reprezentUserPassword.getUsername());
            model.addAttribute("reprezentUserPassword", reprezentUserPassword);
            log.info("Some other errors ");
            return  "user-password";
        } else if (! userService.checkOldPassword(reprezentUserPassword)){
            errors.rejectValue("oldPassword","","Old password was not correct");
            log.info("Old password was not correct");
            return  "user-password";
        }  else if ( reprezentUserPassword.getOldPassword().equals(reprezentUserPassword.getPassword())){
        errors.rejectValue("password","","New and Old password must to be different");
        log.info("Old password was not correct");
        return  "user-password";
    }

        else {
            log.info("Post username" + reprezentUserPassword.getUsername());
//            log.info("Post Old password-->" + reprezentUserPassword.getOldPassword());
            log.info("Post new  password  !!!-->" + reprezentUserPassword.getPassword());
            log.info("Post new  password 2!!!-->" + reprezentUserPassword.getPasswordRepeat());
//            User userPassword = new User();
//            userService.changePassword(encoder.encode(reprezentUserPassword));
            userService.changePassword(reprezentUserPassword);
            return "redirect:/customer";
        }
    }
}
