package com.example.finalprojectlibrary.controler;

import com.example.finalprojectlibrary.Reprezent.ReprezentAdminPassword;
import com.example.finalprojectlibrary.Reprezent.ReprezentUserCreate;
import com.example.finalprojectlibrary.entity.Role;
import com.example.finalprojectlibrary.entity.User;
import com.example.finalprojectlibrary.Reprezent.ReprezentUserData;
import com.example.finalprojectlibrary.service.RoleService;
import com.example.finalprojectlibrary.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
//import org.h2.command.dml.Set;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.HashSet;

//import java.util.HashSet;
import java.util.Set;

@Slf4j
@Controller
@RequiredArgsConstructor
//@SessionAttributes("errors")
@RequestMapping
public class AdminController {

    private final UserService userService;



    @GetMapping("/login")
    public String loginPage() {
        // Getting username of user that is logged in.
        SecurityContextHolder.getContext().getAuthentication().getName();
        return "login";
    }

    @GetMapping("/admin/all")
    public String showAllUsers(Model model) {
        model.addAttribute("userService", userService.getAllUsers());
//        userService.findUser('user').ge
        return "admin-all-user";
    }

    @GetMapping("/admin/{username}")
    public String editUsers(@PathVariable("username") String username, Model model) {
        log.info("username="+username);
        User user = userService.findUser(username);
        log.info("USERNAME--->" + user.getUsername());
        ReprezentUserData reprezentUserData = new ReprezentUserData();
        reprezentUserData.setName(user.getName());
        reprezentUserData.setUsername(user.getUsername());
        reprezentUserData.setEmail(user.getEmail());
        reprezentUserData.setValidUser(user.isValidUser());
        reprezentUserData.setRole(user.getRoles().stream().findFirst().get().getId());
        model.addAttribute("reprezentUserData", reprezentUserData);
        return "admin-edit";
    }


    @PostMapping("/admin/{username}")
    public String editUsersSaveChnages(@PathVariable("username") String username,final  Model model, @Valid ReprezentUserData reprezentUserData, Errors errors) {
        if(errors.hasErrors()){
            model.addAttribute("reprezentUserData", reprezentUserData);
            return "admin-edit";
        }
        else {
            userService.updateUserForAdmin(reprezentUserData);
            return "redirect:/admin/all";
        }
    }


    @GetMapping("/admin/create")
    public String prepearCreateNewUser( Model model) {
        ReprezentUserCreate reprezentUserCreate = new ReprezentUserCreate();
        model.addAttribute("reprezentUserCreate", reprezentUserCreate);
        return "admin-create";
    }

    @PostMapping("/admin/create")
    public String createNewUsers(final Model model, @Valid ReprezentUserCreate reprezentUserCreate, Errors errors) {
        if(userService.findUserExist(reprezentUserCreate.getUsername())){
            errors.rejectValue("username","","User already exist");
            model.addAttribute("reprezentUserCreate", reprezentUserCreate);
            return "admin-create";
        }
        else if(errors.hasErrors()){
            model.addAttribute("reprezentUserCreate", reprezentUserCreate);
            return "admin-create";
        }
        else if(!reprezentUserCreate.getPassword().equals(reprezentUserCreate.getRepeatePassword())){
            errors.rejectValue("password","","Password does not match");
            errors.rejectValue("repeatePassword","","Password does not match");
            model.addAttribute("reprezentUserCreate", reprezentUserCreate);
            return "admin-create";
        }

        else {
            userService.createUserAdmin(reprezentUserCreate);
            return "redirect:/admin/all";
        }
    }



    @GetMapping("/admin/password/{username}")
    public String prepearAdminUserPasswordChange(@PathVariable("username") String username, Model model) {
        ReprezentAdminPassword reprezentAdminPassword = new ReprezentAdminPassword();
        reprezentAdminPassword.setUsername(username);
        model.addAttribute("reprezentAdminPassword", reprezentAdminPassword);
        return "admin-password";
    }


    @PostMapping("/admin/password/{username}")
    public String prepearAdminUserPasswordChange(@PathVariable("username") String username,Model model, @Valid ReprezentAdminPassword reprezentAdminPassword, Errors errors) {

        if(errors.hasErrors()){
            model.addAttribute("reprezentAdminPassword", reprezentAdminPassword);
            return "admin-password";
        }else if(!reprezentAdminPassword.getPassword().equals(reprezentAdminPassword.getPasswordRepeat())){
            errors.rejectValue("password","","Password does not match");
            errors.rejectValue("repeatePassword","","Password does not match");
            model.addAttribute("reprezentAdminPassword", reprezentAdminPassword);
            return "admin-password";
        }else{
            userService.changePasswordAdmin(reprezentAdminPassword);
        }
        return "redirect:/admin/all";
    }


}
