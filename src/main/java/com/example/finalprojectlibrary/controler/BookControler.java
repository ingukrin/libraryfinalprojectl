package com.example.finalprojectlibrary.controler;


import com.example.finalprojectlibrary.service.BooksService;
import com.example.finalprojectlibrary.entity.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.Binding;
import javax.validation.Valid;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequiredArgsConstructor
//@SessionAttributes("errors")
@RequestMapping("/book")
public class BookControler {
    private final BooksService booksService;

//    @GetMapping
//    public String showAllBooks(Model model) {
//        log.info("Starting --> book.all");
//        model.addAttribute("books", booksService.getValidBook());
//        return "book-all";
//    }


    @GetMapping
    public String showAllBooks(Model model,String title, Long bookid) {
        log.info("Starting --> book.all");

        if(title !=  "")
        {
            log.info("Found title --> " + title);
            model.addAttribute("books", booksService.findByTitleAll(title));
        }
        else if(bookid != null){
            model.addAttribute("books", booksService.findByBookIdAll(bookid));
        }
        else
        {
            log.info("Didnt found parametrs");
            model.addAttribute("books", null);
        }
        model.addAttribute("bookList", booksService.getValidBook());
        return "book-all";
    }

    @GetMapping("/{id}")
    public String showBookById(@PathVariable("id") long id, Model model) {
        log.info("Starting --> book.id--> "+id);
        model.addAttribute("book", booksService.getById(id));
        //return "customer-all";
        return "book";
    }

    @GetMapping("/new")
    public String newBook(Model model){
        Book book = new Book();
        book.setYear(2021);
        model.addAttribute("book", book);
        log.info("Created new book object and added in model");
        return "book-new";
    }

    @PostMapping("/new")
      public String createBook(final ModelMap modelMap, @Valid Book book, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        log.info("Recieved data in POST mapping page!!!");
        log.info("Get data-->Book Name:"+ book.getName()+"--> Book year:" + book.getYear()+ " -->Book Author "+ book.getAuthor()+" --> Get  PublishHouse  "+ book.getPublishHouse());
        if(bindingResult.hasErrors()){
            modelMap.addAttribute("book", book);
            return "book-new";
        } else {
            redirectAttributes.addAttribute("xid", booksService.save(book).getBookId());
            return "redirect:/book/{xid}";
        }
    }

// Edit book data
    @GetMapping("/edit/{id}")
    public String editCustomerById(@PathVariable("id") long id,Model model) {
        log.info("Add Book id "+ id + " to model");
        model.addAttribute("book", booksService.getById(id));
        return  "book-edit";
    }
    @PostMapping("/edit/{id}")
    public String updateCustomer(final ModelMap modelMap, @Valid Book book, Errors errors, @PathVariable("id") long id){
//    public String updateCustomer(@ModelAttribute("book") Book book, @PathVariable("id") long id){
        log.info("Recieved data in POST mapping page!!!");
        log.info("Get data--> getName:"+ book.getName()+" --> getBookId: "+ book.getBookId());
        log.info("Get data--> getAuthor:"+ book.getAuthor()+"--> getPublishHouse:" + book.getPublishHouse()+ " -->getYear: "+ book.getYear());

        if(errors.hasErrors()){

            modelMap.addAttribute("book", book);
            modelMap.addAttribute(
                    "errors",
                    errors.getAllErrors().stream()
                            .map(error -> error.getDefaultMessage())
                            .collect(Collectors.toList()));
            return  "book-edit";
        } else {
             booksService.update(book, id);
            return "redirect:/book/{id}";
        }
    }
}
