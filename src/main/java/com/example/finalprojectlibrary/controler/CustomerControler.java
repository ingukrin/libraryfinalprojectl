package com.example.finalprojectlibrary.controler;


import com.example.finalprojectlibrary.service.CustomerService;
import com.example.finalprojectlibrary.entity.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequiredArgsConstructor
//@SessionAttributes("errors")
@RequestMapping("/customer")
public class CustomerControler {
    private final CustomerService custService;

    @GetMapping
    public String showAllCustomers(Model model,String customerName, Long custid) {
        String message="";
        log.info("Starting --> cutomer.all");
        if(customerName !=  "" && customerName !=  null)
        {
            message="Search by customer name";
            log.info("XXXFound customer Name like   --> " + customerName);
            model.addAttribute("customerList", custService.getByName(customerName));
        }
        else if(custid != null){
            message="Search by customer Id";
            log.info("Found customer Id   --> " + custid);
            List<Customer> custList=custService.getById2(custid);
            log.info("Customer service contains data:" + custList.isEmpty());
            model.addAttribute("customerList", custService.getById2(custid));
        }
        else
        {
            message="Print All customers";
            log.info("All is null");
            log.info("Didnt found parametrs");
            model.addAttribute("customerList",  custService.getValidCutomer());
        }
        model.addAttribute("message", message);
        return  "customer-all";
    }

    @GetMapping("/{id}")
    public String showCustomerById(@PathVariable("id") long id,Model model) {
        log.info("Starting --> cutomer.id--> "+id);
        model.addAttribute("customer", custService.getById(id));
        model.addAttribute("rezcust", custService.getById(id).getCutomerRezervations().stream().filter(a->a.getReturnDate()==null).collect(Collectors.toList()));
        return  "customer";
    }


    @GetMapping("/edit/{id}")
    public String editCustomerById(@PathVariable("id") long id,Model model) {
        log.info("Add sutomer id "+ id + " to model");
        model.addAttribute("customer", custService.getById(id));
        return  "customer-edit";
    }

    @PostMapping("/edit/{id}")
//    public String updateCustomer(@ModelAttribute("customer") Customer customer, @PathVariable("id") long id){
     public String updateCustomer(final ModelMap modelMap, @Valid Customer customer, Errors errors , @PathVariable("id") long id){
        log.info("Recieved data in POST mapping page!!!");
        log.info("Get data--> Name:"+ customer.getName()+"--> Email:" + customer.getEmail()+ " -->Valid Customer: "+ customer.getValidCustomer()+" --> CustoemrId: "+ customer.getCustId());
        log.info("Get data--> REGNR:"+ customer.getRegnr()+"--> Phone:" + customer.getPhone()+ " -->Customer City: "+ customer.getCity()+" --> Address: "+ customer.getAddress());
        if(errors.hasErrors()){
            modelMap.addAttribute("customer", customer);
            modelMap.addAttribute(
                    "errors",
                    errors.getAllErrors().stream()
                            .map(error -> error.getDefaultMessage())
                            .collect(Collectors.toList()));

            return "customer-edit";
        } else {
            custService.save(customer);
        }

        //        custService.update(customer, id);
        log.info("Updated data!!!");
        return "redirect:/customer/{id}";
    }

    @GetMapping("/{id}/history")
    public String showCustomerByIdHistory(@PathVariable("id") long id,Model model) {
        log.info("Starting --> cutomer.id--> "+id);
        model.addAttribute("customer", custService.getById(id));
        model.addAttribute("rezcust", custService.getById(id).getCutomerRezervations().stream().filter(a->a.getReturnDate()!=null).collect(Collectors.toList()));
        return  "customer-history";
    }

// Add new customer
    @GetMapping("/new")
    public String newCutomer(Model model){
        model.addAttribute("customer", new Customer());
        model.addAttribute("meassage","");
        log.info("Created new customer object and added in model");
        return "customer-new";
    }


    @PostMapping()
    public String createCustomer(final ModelMap modelMap, @Valid Customer customer, Errors errors, RedirectAttributes redirectAttributes){
        log.info("Recieved data in POST mapping page!!!");
        log.info("Get data--> Name:"+ customer.getName()+"--> Email:" + customer.getEmail()+ " -->Valid Customer "+ customer.getValidCustomer()+" --> CustoemrId  "+ customer.getCustId());
        if(errors.hasErrors()){
            modelMap.addAttribute("customer", customer);
            modelMap.addAttribute(
                    "errors",
            errors.getAllErrors().stream()
                    .map(error -> error.getDefaultMessage())
                    .collect(Collectors.toList()));

             return "customer-new";
            }
        long customerId =custService.save(customer).getCustId();
        log.info("Created new customer with ID + "+ customerId);
        redirectAttributes.addAttribute("customerId", customerId);
        return "redirect:/customer/{customerId}";
    }
// Add new customer



}
