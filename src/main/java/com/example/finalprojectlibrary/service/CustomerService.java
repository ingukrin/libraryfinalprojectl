package com.example.finalprojectlibrary.service;

import com.example.finalprojectlibrary.entity.Customer;
import com.example.finalprojectlibrary.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository curstRepo;

    public List<Customer> getAll(){
        return curstRepo.findAll();
    }

    public List<Customer> getValidCutomer(){
        return curstRepo.findByValidCustomer(true);
    }


    public Customer getById(long id) {
        try {
            return curstRepo.findByCustId(id);
        }catch (Exception e){
            return new Customer();
        }
    }

    public List<Customer> getById2(long id) {
        return curstRepo.findByCustIdAll(id);
    }

    public List<Customer> getByName(String name){
        try {
            return curstRepo.findByNameContaining(name);
        }
        catch (Exception e){
            return new LinkedList<Customer>() ;
        }
    }

    public Customer save(Customer customer){
        return curstRepo.save(customer);
    }


    public Customer update(Customer customer, long id){
        Customer myCustomer = curstRepo.getById(id);
        myCustomer.setName(customer.getName());
        myCustomer.setEmail(customer.getEmail());
        myCustomer.setCity(customer.getCity());
        myCustomer.setAddress(customer.getAddress());
        myCustomer.setPhone(customer.getPhone());
        myCustomer.setValidCustomer(customer.getValidCustomer());
        curstRepo.save(myCustomer);
        return customer;
    }
}
