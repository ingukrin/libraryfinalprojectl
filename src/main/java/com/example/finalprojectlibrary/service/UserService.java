package com.example.finalprojectlibrary.service;

import com.example.finalprojectlibrary.Reprezent.*;
import com.example.finalprojectlibrary.entity.Role;
import com.example.finalprojectlibrary.entity.User;
import com.example.finalprojectlibrary.repository.RoleRepo;
import com.example.finalprojectlibrary.repository.UserRepo;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor

public class UserService {
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final RoleService roleService;

    public List<User> getAllUsers(){
        return userRepo.findAll();
    }

public void create(ReprezentUser reprezentUser){
    User user = new User();
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    user.setUsername(reprezentUser.getUsername());
    user.setEmail(reprezentUser.getEmail());
    user.setName(passwordEncoder.encode(reprezentUser.getPassword()));
    if (reprezentUser.getPassword().equals(reprezentUser.getPasswordRepeat())){
        userRepo.save(user);
    }
}

public User findUser(String userName){
    Optional<User> optionalUser = userRepo.findByUsername(userName);
    return optionalUser.get();
}


    public Boolean findUserExist(String userName){
        User user = userRepo.findByUserNName(userName);
        return user!=null;
    }

public void updateUser(ReprezentUserData userRepr){
    Optional<User> optionalUser = userRepo.findByUsername(userRepr.getUsername());
    User user  = optionalUser.get();
    user.setName(userRepr.getName());
    user.setEmail(userRepr.getEmail());
    user.setValidUser(userRepr.getValidUser());
    userRepo.save(user);
}


    public void updateUserForAdmin(ReprezentUserData userRepr){
        Optional<User> optionalUser = userRepo.findByUsername(userRepr.getUsername());
        User user  = optionalUser.get();
        user.setName(userRepr.getName());
        user.setEmail(userRepr.getEmail());
        user.setValidUser(userRepr.getValidUser());
        user.getRoles().clear();
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(roleService.getById(userRepr.getRole()));
        user.setRoles(roleSet);
        userRepo.save(user);
    }

    public Boolean checkOldPassword(ReprezentUserPassword userRepr){
        Optional<User> optionalUser = userRepo.findByUsername(userRepr.getUsername());
        User user  = optionalUser.get();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(userRepr.getOldPassword(),user.getPassword() );
    }


    public void changePassword(ReprezentUserPassword userRepr){
        Optional<User> optionalUser = userRepo.findByUsername(userRepr.getUsername());
        User user  = optionalUser.get();
        if( userRepr.getPassword().equals(userRepr.getPasswordRepeat())) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            user.setPassword(passwordEncoder.encode(userRepr.getPassword()));
            userRepo.save(user);
        }
        else{
            log.warn("Password was not saved");
        }
    }

    public void changePasswordAdmin(ReprezentAdminPassword reprezentAdminPassword){
        Optional<User> optionalUser = userRepo.findByUsername(reprezentAdminPassword.getUsername());
        User user  = optionalUser.get();
        if( reprezentAdminPassword.getPassword().equals(reprezentAdminPassword.getPasswordRepeat())) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            user.setPassword(passwordEncoder.encode(reprezentAdminPassword.getPassword()));
            userRepo.save(user);
        }
        else{
            log.warn("Password was not saved");
        }
    }

    public void createUser(User user){
       userRepo.save(user);
    }

    public void createUserAdmin(ReprezentUserCreate reprezentUserCreate){
        User user = new User();
        user.setUsername(reprezentUserCreate.getUsername());
        user.setEmail(reprezentUserCreate.getEmail());
        user.setName(reprezentUserCreate.getName());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(reprezentUserCreate.getPassword()));
        user.setValidUser(reprezentUserCreate.getValidUser());
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(roleService.getById(reprezentUserCreate.getRole()));
        user.setRoles(roleSet);
        userRepo.save(user);
    }
}
