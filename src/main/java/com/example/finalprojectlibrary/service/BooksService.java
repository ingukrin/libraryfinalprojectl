package com.example.finalprojectlibrary.service;

import com.example.finalprojectlibrary.entity.Book;
import com.example.finalprojectlibrary.repository.BooksRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class BooksService {
    private final BooksRepository bookRepo;

    public List<Book> getAll(){
        return bookRepo.findAll();
    }

    public List<Book> getValidBook(){
        return bookRepo.findByValidBook(true);
    }

    public Book getById(long id) {
        log.info("Tring to find book with id:" + id);
        return bookRepo.findByBookId(id);
        //  .orElseThrow(() -> new ModelException("File data with id:" + id + " not found!###################################################################"));
    }

    public Book save(Book book){
        return bookRepo.save(book);
    }


    public List<Book> findByBookIdWioutRezervations(Long id){
        log.info("Fing book like for book id --> "+ id);
        return bookRepo.findByBookIdWioutRezervations(id);
    }
    public List<Book> findByBookIdAll(Long id){
        log.info("Fing book like for book id --> "+ id);
        return bookRepo.findByBookIdAll(id);
    }

    public List<Book> findByTitleIdWioutRezervations(String title){
        log.info("Fing book for book title --> "+ title);
        return bookRepo.findByTitleIdWioutRezervations(title);
    }

    public List<Book> findByTitleAll(String title){
        log.info("Fing book for book title --> "+ title);
        return bookRepo.findByTitleAll(title);
    }

    public Book update(Book book, long id){
        Book myBook = bookRepo.getById(id);
        log.info("Book id " + myBook.getBookId());
        myBook.setName(book.getName());
        myBook.setYear(book.getYear());
        myBook.setPublishHouse(book.getPublishHouse());
        myBook.setAuthor(book.getAuthor());
        myBook.setValidBook(book.isValidBook());
        bookRepo.save(myBook);
        return myBook;
    }


}
