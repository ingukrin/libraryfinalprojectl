package com.example.finalprojectlibrary.service;

import com.example.finalprojectlibrary.entity.Book;
import com.example.finalprojectlibrary.entity.Role;
import com.example.finalprojectlibrary.repository.BooksRepository;
import com.example.finalprojectlibrary.repository.RoleRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepo roleRepo;

    public Role getById(long id) {
        return roleRepo.findById(id).get();
    }



}
