package com.example.finalprojectlibrary.service;

public class ServiceException extends RuntimeException{
    public ServiceException(final String message) {
        super(message);
    }

}
